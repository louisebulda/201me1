//
// ME201.swift
// ME201
//
// Created by Lourdes Bulda on 10/10/2021
//

class Food {
  var name: String

   enum Size {
    case meal (price: Double)
    case solo (price: Double)

    var sizeString: String {
      switch self {
        case .meal: return "Meal"
        case .solo: return "Solo"
      }
    }

    var priceDouble: Double {
      switch self {
        case .meal (let price): return price
        case .solo (let price): return price
      }
    }
  }

  var price: Size

  init(name: String, price: Size) {
    self.name = name
    self.price = price
  }
  
  func printDetails() {
    print("Ordered \(name) (\(price.sizeString)) for P\(price.priceDouble)")
  }
}

class Beverage {
  var name: String

   enum Size {
    case small (price: Double)
    case medium (price: Double)
    case large (price: Double)

    var sizeString: String {
      switch self {
        case .small: return "S"
        case .medium: return "M"
        case .large: return "L"
      }
    }

    var priceDouble: Double {
      switch self {
        case .small (let price): return price
        case .medium (let price): return price
        case .large (let price): return price
      }
    }
  }

  var price: Size

  init(name: String, price: Size) {
    self.name = name
    self.price = price
  }
  
  func printDetails() {
    print("Ordered \(name) (\(price.sizeString)) for P\(price.priceDouble)")
  }
}

// Get orders (food and beverages separated)
class OrderTaker {
   var orderFood = [Food]()
   var orderBev = [Beverage]()

   func addFood(food: Food) {
     orderFood.append(food)
     food.printDetails()
   }

   func addBeverage(beverage: Beverage) {
     orderBev.append(beverage)
     beverage.printDetails()
   }

   func orderDetails(food: Food, beverage: Beverage) {
     food.printDetails()
     beverage.printDetails()
   }

   func pay(cash: Double) {
     let foodOrder = orderFood.map { $0.price.priceDouble }
     let bevOrder = orderBev.map { $0.price.priceDouble }
     let foodSum = foodOrder.reduce(0, +)
     let bevSum = bevOrder.reduce(0, +)
     let total = foodSum + bevSum

     if (cash < total) {
       print("\n-------------------------------")
       print("Insufficient payment.")
     }
     else {
       func printDetails() {
         print("\n-------------------------------")
         print("THIS IS NOT AN OFFICIAL RECEIPT")
         print("\nITEMS ....... PRICE")

         for i in orderFood {
           print("\(i.name) (\(i.price.sizeString)) .... P\(i.price.priceDouble)")
         }

         for n in orderBev {
           print("\(n.name) (\(n.price.sizeString)) .... P\(n.price.priceDouble)")
         }

         let change: Double = cash - total

         print("\nTOTAL .... P\(total)")
         print("CASH ....... P\(cash)")
         print("CHANGE ..... P\(change)")
         print("---------------------------------")
       }

        printDetails()
     }
   }
}


// Sample Orders
let o = OrderTaker()
o.addFood(food: Food(name: "Spaghetti", price: .solo(price: 125.0)))
o.addBeverage(beverage: Beverage(name: "Iced Tea", price: .large(price: 55.0)))
o.addBeverage(beverage: Beverage(name: "Orange Juice", price: .medium(price: 45.0)))
o.addFood(food: Food(name: "Cheeseburger", price: .meal(price: 200.0)))
o.pay(cash: 1000.0)





